//
//  ReimbrsmntVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 27/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import Alamofire

class ReimbrsmntVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBAction func btnClaim(_ sender: Any) {
    }
    
    @IBAction func btnStatus(_ sender: Any) {
        let RmbrsmntStatus =
            self.storyboard?.instantiateViewController(withIdentifier: "ReimbrsmntStatusVC") as! ReimbrsmntStatusVC
        self.navigationController?.pushViewController(RmbrsmntStatus, animated: true)
     
    }
    
    
    @IBOutlet weak var rmbrsmntTble: UITableView!
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    // API-array
    var dictClaimRmbrsmnt = NSArray()

    
    override func viewDidLoad() {
        super.viewDidLoad()
       RMBRSMNT_ClaimApiCall()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dictClaimRmbrsmnt.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = rmbrsmntTble.dequeueReusableCell(withIdentifier: "rmbrsmnt_tabl_cell", for: indexPath) as! rmbrsmnt_tabl_cell
        
        
        let dataDict = dictClaimRmbrsmnt.object(at: indexPath.row) as? NSDictionary ?? [:]

        
        let client_name = dataDict["client_name"] as? String
        var str = "Client Name: "
        if let _ = client_name {
            str.append(client_name!)
        }
        cell.lblClientName.text = str
        
        let location = dataDict["location"] as? String
        
        print("Location is", location)
        var str2 = "Location: "
        if let _ = location {
            str2.append(location!)
        }
        cell.lblLocation.text = str2
        
        
        
        let meeting_time = dataDict["meeting_time"] as? String
        var str3 = "Time: "
        if let _ = meeting_time {
            str3.append(meeting_time!)
            
        }
        cell.lblTime.text = str3
        
        return cell
    }
    
    
    
    
    func RMBRSMNT_ClaimApiCall() {
        
        if Reachability.isConnectedToNetwork() {
            
            var UserId = UserDefaults.standard.value(forKey: "USERID")
            
            let passDictRmrbsmnt = [
                "user_id": UserId
                ] as [String : Any]
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/salesforce-dev/public/api/getReimbursements", method: .post, parameters: passDictRmrbsmnt, encoding: JSONEncoding.default )
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("msg",JSON)
                            self.dictClaimRmbrsmnt = JSON["meeting_list"] as? NSArray ?? []
                            print("THE RESPONSE Reimbursement",self.dictClaimRmbrsmnt)

                            self.rmbrsmntTble.reloadData()

                            
                            //self.arrayRef = JSON["summary"] as? NSArray ?? []
                            //print("THE ARRAY", self.arrayRef)
                            //self.summaryTable.reloadData()
                        }
                        
                    case .failure(let error):
                        print("ERROR", error.localizedDescription)
                        self.hideActivityIndicator()
                    }
                    
                    
            }
            
            
        }
    
    }
    
    
    
    
}
