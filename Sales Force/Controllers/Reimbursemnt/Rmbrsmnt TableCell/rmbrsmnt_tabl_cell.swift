//
//  rmbrsmnt_tabl_cell.swift
//  Sales Force
//
//  Created by Syed Zuber on 27/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class rmbrsmnt_tabl_cell: UITableViewCell {

    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblClientName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblNotClaimed: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
