//
//  rmbrsmntStatusTbleCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 28/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class rmbrsmntStatusTbleCell: UITableViewCell {
    
    
    @IBOutlet weak var lblClientName: UILabel!
    
    @IBOutlet weak var lblLocation: UILabel!
    
    
    @IBOutlet weak var lblClaimDate: UILabel!
    
    
    @IBOutlet weak var lblAmount: UILabel!
    
    
    @IBOutlet weak var lblStatus: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
