//
//  rmbrsmntDetailTbleCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 29/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class rmbrsmntDetailTbleCell: UITableViewCell {

    
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var lblClientName: UILabel!
   
    @IBOutlet weak var lblLocation: UILabel!
   
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var imgStatus: UIImageView!
    
    
    
    @IBOutlet weak var viewBillDetails: UIView!
    
    @IBOutlet weak var lblBillName: UILabel!
   
    @IBOutlet weak var lblBillNumber: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var lblIMGStatus: UILabel!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
