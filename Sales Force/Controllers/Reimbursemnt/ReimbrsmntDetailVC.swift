//
//  ReimbrsmntDetailVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 29/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import Alamofire

class ReimbrsmntDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

    var arrayRef = NSArray()
    var arrayBill = NSArray()
    
    @IBOutlet weak var detailRbrsmntTble: UITableView!
    
    @IBAction func btnBack(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       getReImburseDetailAPI()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRef.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rmbrsmntDetailTbleCell", for: indexPath) as! rmbrsmntDetailTbleCell
        
        
        let dataDict = arrayRef.object(at: indexPath.row) as? NSDictionary ?? [:]
        
        let billDict = arrayBill.object(at: indexPath.row) as? NSDictionary ?? [:]
        
        
        
        let client_name = dataDict["client_name"] as? String
        var str = "Client Name: "
        if let _ = client_name {
            str.append(client_name!)
        }
        cell.lblClientName.text = str
        
        
        //let location = meetingDetailDict["location"] as? String
        let location = dataDict["location"] as? String
        print("Location is", location)
        var str2 = "Location: "
        if let _ = location {
            str2.append(location!)
        }
        cell.lblLocation.text = str2
        
        
        let date_time = dataDict["date_time"] as? String
        var str3 = "date_time: "
        if let _ = date_time {
            str3.append(date_time!)
        }
        cell.lblDate.text = str3
        
        
        let bill_name = billDict["bill_name"] as? String
        var str4 = "Bill Name: "
        if let _ = bill_name {
            str4.append(bill_name!)
        }
        cell.lblBillName.text = str4
        
        
        
        let bill_number = billDict["bill_number"] as? String
        var str5 = "Bill NO: "
        if let _ = bill_number {
            str5.append(bill_name!)
        }
        cell.lblBillNumber.text = str5
        
        
        let amount = billDict["amount"] as? String
        var str6 = "₹: "
        if let _ = amount {
            str6.append(amount!)
        }
        cell.lblAmount.text = str6
        
        
        
        let amount2 = billDict["amount"] as? String
        var str7 = "Total Amount: "
        if let _ = amount2 {
            str7.append(amount2!)
        }
        cell.lblTotalAmount.text = str7
        
        return cell
    }
    
    
    func getReImburseDetailAPI() {
        
        
        if Reachability.isConnectedToNetwork() {
            
            var UserId = UserDefaults.standard.value(forKey: "USERID")
            var meetingID = 1005
            
            let passDictRmbrsmnt = [
                
                "user_id": UserId,
                "meeting_id": meetingID
                
                ] as [String : Any]
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/salesforce-dev/public/api/getReImburseDetail", method: .post, parameters: passDictRmbrsmnt, encoding: JSONEncoding.default )
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("msg",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                self.arrayRef = JSON["details"] as? NSArray ?? []
                                print("THE ARRAY", self.arrayRef)
                                self.detailRbrsmntTble.reloadData()
                                
                                self.arrayBill = JSON["bills"] as? NSArray ?? []
                                print("THE BILLS ARE", self.arrayBill)
                                
                            }
                            
                        }
                        
                    case .failure(let error):
                        print("ERROR")
                        self.hideActivityIndicator()
                    }
                    
                    
            }
            
            
        }
    }
}
