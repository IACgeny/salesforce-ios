//
//  ReimbrsmntStatusVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 28/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import Alamofire

class ReimbrsmntStatusVC: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
   
    
    @IBAction func btnClaim(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnStatus(_ sender: Any) {
        
    }
    
    @IBOutlet weak var rmbrsmntStatusTble: UITableView!
    

    var dictStatusRmbrsmnt = NSArray()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        RMBRSMNT_StatusApiCall()
        self.rmbrsmntStatusTble.dataSource = self
        self.rmbrsmntStatusTble.delegate = self
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictStatusRmbrsmnt.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = rmbrsmntStatusTble.dequeueReusableCell(withIdentifier: "rmbrsmntStatusTbleCell", for: indexPath) as! rmbrsmntStatusTbleCell
        
        

        let dataDict = dictStatusRmbrsmnt.object(at: indexPath.row) as? NSDictionary ?? [:]
        
        
        let client_name = dataDict["client_name"] as? String
        var str = "Client Name: "
        if let _ = client_name {
            str.append(client_name!)
        }
        cell.lblClientName.text = str
        
        let location = dataDict["location"] as? String
        
        print("Location is", location)
        var str2 = "Location: "
        if let _ = location {
            str2.append(location!)
        }
        cell.lblLocation.text = str2
        
        
        
        let meeting_time = dataDict["claim_date"] as? String
        var str3 = "Claim date: "
        if let _ = meeting_time {
            str3.append(meeting_time!)
            
        }
        cell.lblClaimDate.text = str3
        
        let total_amount = dataDict["total_amount"] as? String
        var str4 = "₹ "
        if let _ = total_amount {
            str4.append(total_amount!)
            
        }
     
        cell.lblAmount.text = str4
        
        cell.lblStatus.text = dataDict["status"] as? String
      
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let DetailReimbrsmntSC = self.storyboard?.instantiateViewController(withIdentifier: "ReimbrsmntDetailVC") as! ReimbrsmntDetailVC
        self.navigationController?.pushViewController(DetailReimbrsmntSC, animated: true)
    }
    
    func RMBRSMNT_StatusApiCall() {
        
        
        var UserId = UserDefaults.standard.value(forKey: "USERID")
        
        let passDictstatus = [
            "user_id": UserId
            ] as [String : Any]
        
        showActivityIndicator()
        
        Alamofire.request("http://13.232.6.230/salesforce-dev/public/api/getImbursementStatus", method: .post, parameters: passDictstatus, encoding: JSONEncoding.default )
            
            .responseJSON { (response) in
                
                switch response.result {
                    
                case .success:
                    if let JSON = response.result.value as? [String: Any] {
                        
                        self.hideActivityIndicator()
                        
                        print("msg",JSON)
                        self.dictStatusRmbrsmnt = JSON["list"] as? NSArray ?? []
                        print("THE RESPONSE Reimbursement",self.dictStatusRmbrsmnt)
                        
                        self.rmbrsmntStatusTble.reloadData()
                        
                        
                    }
                    
                case .failure(let error):
                    print("ERROR", error.localizedDescription)
                    self.hideActivityIndicator()
                }
                
        }
        
        
        
    }

}
