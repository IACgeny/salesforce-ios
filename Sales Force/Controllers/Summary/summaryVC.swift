//
//  summaryVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 17/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import Alamofire

class summaryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
 
    @IBOutlet weak var summaryView: UIView!

    @IBOutlet weak var summaryTable: UITableView!
  
    var meetingDetailDict = NSDictionary()
    
    var arrayRef = NSArray()

    /*var arrayRef:[Any] = []
    var meetingArray:[String] = []
    var btnTag:[Int] = []*/

    //var nameArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        summaryApiCall()
    }
    
   
    //Mark:- Summary TableView
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayRef.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "summaryTableCell", for: indexPath) as! summaryTableCell
        
        
        let dataDict = arrayRef.object(at: indexPath.row) as? NSDictionary ?? [:]
        
        //-->
        self.meetingDetailDict = dataDict["meetings"] as? NSDictionary ?? [:]
        print("THE MEETHING", self.meetingDetailDict)
        
        
         let client_name = dataDict["client_name"] as? String
        var str = "Client Name: "
        if let _ = client_name {
            str.append(client_name!)
        }
        cell.lblClientName.text = str
       
        
        //-->
        let location = meetingDetailDict["location"] as? String
     
        print("Location is", location)
        var str2 = "Location: "
        if let _ = location {
            str2.append(location!)
        }
        cell.lblLocation.text = str2
        
       
        let designation = dataDict["designation"] as? String
        var str3 = "Designation: "
        if let _ = designation {
            str3.append(designation!)
        }
        cell.lblDesignation.text = str3
        
        
        let date_time = meetingDetailDict["date_time"] as? String
        var str4 = "Time: "
        if let _ = date_time {
             str4.append(date_time!)
        }
        cell.lblDate.text = str4
       
        cell.btnSummaryDetails.addTarget(self, action: #selector(onBtnSummaryDetailsClicked(_:)), for:.touchUpInside)
        
        return cell
    }

    @objc func onBtnSummaryDetailsClicked(_ sender:UIButton){
        
        
    }

    func summaryApiCall() {
        if Reachability.isConnectedToNetwork() {
        
        var UserId = UserDefaults.standard.value(forKey: "USERID")
        
        let passDictMeeting = [
            
            "user_id": UserId
            ] as [String : Any]
        
        showActivityIndicator()
        
        Alamofire.request("http://13.232.6.230/SalesForce/public/api/getMeetingSummary", method: .post, parameters: passDictMeeting, encoding: JSONEncoding.default )
            
            .responseJSON { (response) in
                
                switch response.result {
                    
                case .success:
                    if let JSON = response.result.value as? [String: Any] {
                        
                        self.hideActivityIndicator()
                        
                        print("msg",JSON)
                        let message = JSON["status"] as? String ?? ""
                        print(message)
                        if message == "success" {
                            
                            self.hideActivityIndicator()
                            
                            self.arrayRef = JSON["summary"] as? NSArray ?? []
                            print("THE ARRAY", self.arrayRef)
                            self.summaryTable.reloadData()
                        }
                        
                    }
                    
                case .failure(let error):
                    print("ERROR")
                    self.hideActivityIndicator()
                }
                
                
    }
    
    
}
    }
}

