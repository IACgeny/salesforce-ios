//
//  formSummaryTableCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 26/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class formSummaryTableCell: UITableViewCell {

    
    @IBOutlet weak var btnSelectDevice: UIButton!
    
    @IBOutlet weak var lblQuantity: UILabel!
    
    @IBOutlet weak var `switch`: UISwitch!
   
    @IBOutlet weak var btnAddCell: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
