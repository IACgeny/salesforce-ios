//
//  summaryTableCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 17/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class summaryTableCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblClientName: UILabel!
    
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var lblDesignation: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var btnSummaryDetails: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
