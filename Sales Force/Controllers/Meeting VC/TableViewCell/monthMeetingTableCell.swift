//
//  monthMeetingTableCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 21/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class monthMeetingTableCell: UITableViewCell {
    
    
    @IBOutlet weak var lblMtngDetails: UILabel!
    @IBOutlet weak var lblAttdngNtAAttng: UILabel!
    @IBOutlet weak var lblClientName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    
    @IBOutlet weak var btnAddSmmry: UIButton!
    
    @IBOutlet weak var btnAtnding: UIButton!
    
    
    @IBOutlet weak var btnNotAtndng: NSLayoutConstraint!
    
    @IBOutlet weak var btnNotAttending: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
