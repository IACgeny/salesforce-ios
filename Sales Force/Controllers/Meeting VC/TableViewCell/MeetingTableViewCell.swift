//
//  MeetingTableViewCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 17/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class MeetingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var meetingDetails: UILabel!
    @IBOutlet weak var attndngNotAttndng: UILabel!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var timeDay: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var subjectOrAgenda: UILabel!
    @IBOutlet weak var addSummary: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
            
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
