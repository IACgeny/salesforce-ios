//
//  AgendaTableCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 28/10/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class AgendaTableCell: UITableViewCell {

    
    @IBOutlet weak var lblType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
