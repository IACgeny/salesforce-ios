//
//  MeetingVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 17/10/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import DropDown
import Alamofire

class MeetingVC: UIViewController {
    
    //MARK:- IBOUTLET
    @IBOutlet weak var txtMeetings: UITextField!
    @IBOutlet weak var txtMeetingAgenda: UITextField!
    
    @IBOutlet weak var enterName: UITextField!
    @IBOutlet weak var addLocation: UITextField!
    @IBOutlet weak var selectDate: UITextField!
    @IBOutlet weak var selectTime: UITextField!
    

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var popUpView: UIView!
    
    
    
    //MARK:- USERDEFINE DATA
    let dropDown = DropDown()
    
    var membersNameArray = NSArray()
    var  agendaArray = NSArray()
    
    var nameArr = NSMutableArray()
    
    var idArray = NSMutableArray()
    
    //MARK:- UIVIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        membersApiCall()
        agendaApi()
        
        popUpView.isHidden = true
    }
    
    
    @IBAction func btnDropDown(_ sender: Any) {
        //Data Coming from API integrate it later.
        
        dropDown.show()
        
        dropDown.dataSource = ["Select Meeting to", "Corporates", "Dealers", "Distributors", "Logistic Companies", "Showroom", "Individual", "Others"]
        dropDown.direction = .bottom
        dropDown.anchorView = self.txtMeetings as! AnchorView
        dropDown.bottomOffset = CGPoint(x: 0, y: txtMeetings.bounds.height)
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtMeetings.text = item
        }
        
    }
    
    
  
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func meetingAgenda(_ sender: Any) {
        popUpView.isHidden = false
    }
    
    
    @IBAction func btnCancelPopUp(_ sender: Any) {
        popUpView.isHidden = true
    }
    
    @IBAction func btnOkPopUp(_ sender: Any) {
        
        let name = self.nameArr.componentsJoined(by: ",")
        print("THE BAME STIRNG", name)
 //       print("TEH DAT ARRAY", self.agendaArray)
        
        
      self.txtMeetingAgenda.text = name
        popUpView.isHidden = true
    }
    
    
    @objc func handleAction(sender: UIButton) {
        
        guard (tableView.dequeueReusableCell(withIdentifier: "AgendaTableCell") as? AgendaTableCell) != nil else { return }
        
        print("\(sender.tag)")
        
        
    }
    
    // MARK:-  CREATE Meeting API on SAVE button
    
    @IBAction func saveAddMtng(_ sender: Any) {
        
        let UserId = UserDefaults.standard.value(forKey: "USERID")
        
        let Latitude = 28.644800
        let Longitude = 77.216721
        
        let memberId = "\(3)"
        let otherAgenda = "AGENDA"
        let otherMeeting = "MEETINGTOOTHER"
        let Detail = "testing iOS"
        
        let passDictAddMeeting = [
            "user_id": UserId as Any,
            "member_id": "\(memberId)",
            "client_name": enterName.text! ,
            "volume": "\(28)",
            "detail" : Detail ,
            "meeting_agenda": idArray,
            "agenda_other": otherAgenda,
            "meeting_to_other": otherMeeting,
            "meeting_to": txtMeetings.text! ,
            "location": addLocation.text! ,
            "latitude": "\(Latitude)" ,
            "longitude": "\(Longitude)" ,
            "date_time": selectDate.text!,
            "to_time": ""
             ] as [String : Any]
            
            if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
                
            Alamofire.request("http://13.232.6.230/salesforce-dev/public/api/createMeeting", method: .post, parameters: passDictAddMeeting, encoding: JSONEncoding.default )
            .responseJSON { (response) in
            
            switch response.result {
            
            case .success:
            if let JSON = response.result.value as? [String: Any] {
            
            self.hideActivityIndicator()
            
            print("THE JSON RESULT",JSON)
            let message = JSON["status"] as? String ?? ""
            print("THE MESSAGE IS",message)
            if message == "success" {
            
            self.hideActivityIndicator()
                
                 let meetingScreen = self.storyboard?.instantiateViewController(withIdentifier: "MeetingScreenVC") as! MeetingScreenVC
                self.navigationController?.pushViewController(meetingScreen, animated: true)
                
               
            }
            
            }
            
            case .failure(let error):
            print("ERROR", error.localizedDescription)
            self.hideActivityIndicator()
                self.alert(message: "Something went Wrong, Please Try Again", title: "Sales Force Alert!!")
                }
              
             }
         }
            else {
                self.hideActivityIndicator()
                alert(message: "No Internet Connection.", title: "Sales Force Alert!!!")
        }
        
      }
    
    func validationsTextField() {
    
        if (enterName.text?.isEmpty == true) &&
            (addLocation.text?.isEmpty == true) &&
            (selectDate.text?.isEmpty == true) &&
            (selectTime.text?.isEmpty == true) {
             alert(message: "Please Fill Details", title: " Sales Force Alert!!")
        }
        else if (enterName.text?.isEmpty == true) {
            alert(message: "please fill name", title: "sales force alert!!")
    }
        else if (addLocation.text?.isEmpty == true) {
            alert(message: "Please add Location", title: "Sale Force Alert!!")
        }
        
        else if (selectDate.text?.isEmpty == true) {
            alert(message: "Please elect Date", title: "Slaes Force Alert!! ")
        }
        
        
    }
    
   }



//MARK:- UICollectionView Delegate and DataSource
//MARK:-
extension MeetingVC: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return membersNameArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MeetingCollectionViewCell", for: indexPath) as! MeetingCollectionViewCell
        
        let dataDict = membersNameArray.object(at: indexPath.row) as? NSDictionary ?? [:]
        
        print("DATA IN DICTIONARY", dataDict)
        
        cell.lblName.text = dataDict.value(forKey: "username") as? String ?? "Abhishek"
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("Cell tabbed")
    }
}

//MARK:- TableView DataSource and Delegate
//MARK:-
extension MeetingVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return agendaArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AgendaTableCell", for: indexPath) as! AgendaTableCell
 
        let dataDict = agendaArray.object(at: indexPath.row) as? NSDictionary ?? [:]
        cell.lblType.text = dataDict.value(forKey: "name") as? String ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataDict = agendaArray.object(at: indexPath.row) as? NSDictionary ?? [:]

        if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
            
            self.nameArr.remove(dataDict.value(forKey: "name") as? String ?? "")
            self.idArray.remove(dataDict.value(forKey: "id") as? Int ?? 0)
        }
        else {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            self.nameArr.add(dataDict.value(forKey: "name") as? String ?? "")
            self.idArray.add(dataDict.value(forKey: "id") as? Int ?? 0)
        }

     //   print("The Value in Array", self.nameArr)
        print("THE ID ARRAY", self.idArray)
    }
    
    
    func membersApiCall() {
        
        if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
            
             Alamofire.request("http://13.232.6.230/SalesForce/public/api/membersList", method: .post, parameters: nil, encoding: JSONEncoding.default )
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("msg",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                self.membersNameArray = JSON["members"] as? NSArray ?? []
                                print("THE ARRAY", self.membersNameArray)
                                self.collectionView.reloadData()
    }
    
}

                    case .failure(let error):
                        print("ERROR", error.localizedDescription)
                        self.hideActivityIndicator()
                    }
                    
                
                    
            }
        }
    }
    
    func agendaApi() {
        
        if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/getMeetingAgenda", method: .post, parameters: nil, encoding: JSONEncoding.default )
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("msg",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                                self.hideActivityIndicator()
                                
                                self.agendaArray = JSON["agenda_for_create_meeting"] as? NSArray ?? []
                                print("THE ARRAY", self.agendaArray)
                        
                            self.tableView.reloadData()
                        }
                        
                    case .failure(let error):
                        print("ERROR", error.localizedDescription)
                        self.hideActivityIndicator()
                    }
                    
                    
                    
            }
        }
    
    }
    
    
}

