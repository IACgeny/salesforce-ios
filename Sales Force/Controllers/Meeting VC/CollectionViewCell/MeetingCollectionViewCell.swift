//
//  MeetingCollectionViewCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 17/10/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class MeetingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    
}
