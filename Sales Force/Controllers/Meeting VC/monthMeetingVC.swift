//
//  monthMeetingVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 21/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import DropDown
import Alamofire

class monthMeetingVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var mnthMeetingView: UIView!
    @IBOutlet weak var mnthMtngTble: UITableView!
    @IBOutlet weak var lblMonth: UILabel!
    

    
    let dropDown = DropDown()
    var monthIndex: Int?
    let userID = UserDefaults.standard.value(forKey: "USERID")
    var meetingDataArray = NSArray()
    var meetingStatus = ""
    
    var attendingArray = NSArray()
    var notAttendingArray = NSArray()
    
    
    @IBAction func btnBack(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblMonth.text = "Select Month"
        
     }
//    override func viewWillAppear(_ animated: Bool) {
//        if meetingDataArray.count == 0{
//            self.mnthMeetingView.isHidden = true
//        }else{
//            self.mnthMeetingView.isHidden = false
//        }
//        
//        self.mnthMtngTble.reloadData()
//        
//    }
    
    @IBAction func btnDropDown(_ sender: Any) {
        showDropDown()
        
    }
    
 
    func showDropDown() {
        
        dropDown.show()
        
        dropDown.dataSource = ["Select Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        dropDown.direction = .bottom
        dropDown.anchorView = self.lblMonth as! AnchorView
        dropDown.bottomOffset = CGPoint(x: 0, y: lblMonth.bounds.height)
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblMonth.text = item
            self.monthIndex = index
            if self.monthIndex == nil {
                self.monthIndex = 0
            }
            else if self.monthIndex == 0 {
                print("ERROR")
            }
            else {
                self.meetingsByMonthAPI()
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return meetingDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = mnthMtngTble.dequeueReusableCell(withIdentifier: "monthMeetingTableCell", for: indexPath) as! monthMeetingTableCell
        
        if meetingDataArray.count >= 0 {
            let dataDict = self.meetingDataArray.object(at: indexPath.row) as? NSDictionary ?? [:]
            
            cell.lblClientName.text = "Client Name : \(dataDict.value(forKey: "client_name") as? String ?? "")"
            cell.lblTime.text = "Time : \(dataDict.value(forKey: "date_time") as? String ?? "")"
            cell.lblLocation.text = "Location : \(dataDict.value(forKey: "location") as? String ?? "")"
            cell.lblSubject.text = "Subject : \(dataDict.value(forKey: "meeting_agenda") as? String ?? "")"
            cell.lblAttdngNtAAttng.text = " \(dataDict.value(forKey: "meeting_status") as? String ?? "")"
            
            self.meetingStatus = dataDict.value(forKey: "meeting_status") as? String ?? ""
            print("THE VALUE OF MEETING STATUS", self.meetingStatus)
            
            if self.meetingStatus == "Attending" {
                print("HIDE THE Attending & Not attending BUTTON")
                cell.btnAtnding.isHidden = true
                cell.btnNotAttending.isHidden = true
                cell.btnAddSmmry.isHidden = false
            }
            else if self.meetingStatus == "Not attending" {
                print("HIDE THE Attending & Not attending BUTTON")
                cell.btnAtnding.isHidden = true
                cell.btnNotAttending.isHidden = true
                cell.btnAddSmmry.isHidden = false
            }
            else {
                print("Show THE Attending & Not attending BUTTON")
                cell.btnAtnding.isHidden = false
                cell.btnNotAttending.isHidden = false
                cell.btnAddSmmry.isHidden = true
            }
            
            
            cell.btnAtnding.addTarget(self, action: #selector(onbtnAtndingClicked(_:)), for:.touchUpInside)
            
            
            cell.btnNotAttending.addTarget(self, action: #selector(onbtnNotAttendingClicked(_:)), for:.touchUpInside)

            
            
            //REMOVE THAT CODE FROM HERE
       //     cell.btnAddSmmry.tag = indexPath.row
       //     cell.btnAddSmmry.addTarget(self, action: Selector(("buttonClicked:")), for: .touchUpInside)

        }
        else {
            print("ERROR")
        }

        return cell
    }
    
    
    
    @objc func onbtnAtndingClicked(_ sender:UIButton){
       attendMeetingAPI()
    }
    

    
    @objc func onbtnNotAttendingClicked(_ sender:UIButton){
        notAttendMeetingAPI()
        
    }
    
    
    //REMOVE THAT CODE FROM HERE
//    func buttonClicked(sender:UIButton) {
//
//        let buttonRow = sender.tag
//        print("Button Row", buttonRow)
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 180
    }
    
    
    
    func meetingsByMonthAPI() {
        
        let passDict = [
            "user_id": userID,
            "month":monthIndex] as [String : Any]
        
        
        if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/meetingsByMonth", method: .post, parameters: passDict, encoding: JSONEncoding.default)
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                           print("THE JSON RESULT",JSON)
                            
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                self.meetingDataArray = JSON["result"] as? NSArray ?? []
                                
                                print("THE DATA ARRAY IS",self.meetingDataArray)
                               
                                if self.meetingDataArray.count == 0 {
                                    self.mnthMeetingView.isHidden = true
                                    self.alert(message: "NO MEETINGS FOUND!!")
                                }
                                else {
                                    self.mnthMeetingView.isHidden = false
                                }
                        self.mnthMtngTble.reloadData()
                            }
                                
                            else {
                                self.hideActivityIndicator()
                                
                                self.alert(message: JSON["msg"] as? String ?? "Invalid Creditions", title: "Sale Force Alert!!")
                                print("print error")
                            }
                        }
                    case .failure(let error):
                        print("ERROR", error.localizedDescription)
                        self.hideActivityIndicator()
                        
                        self.alert(message: "Something went Wrong, Please Try Again", title: "Sales Force Alert!!")
                    }
                    
            }
        }
        else {
            self.hideActivityIndicator()
            alert(message: "No Internet Connection.", title: "Sales Force Alert!!!")
        }
        
      
        
    }
    
    func attendMeetingAPI() {
        if Reachability.isConnectedToNetwork() {
            
            var UserId = UserDefaults.standard.value(forKey: "USERID")
            
            let passDictAttending = [
                
                "user_id": UserId,
                "meeting_id":1673,
                "latitude":75.02,
                "longitude":25.22

                ] as [String : Any]
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/attendMeeting", method: .post, parameters: passDictAttending, encoding: JSONEncoding.default )
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("msg",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                self.attendingArray = JSON[""] as? NSArray ?? []
                                print("THE ARRAY", self.attendingArray)
                                
                            }
                            
                        }
                        
                    case .failure(let error):
                        print("ERROR")
                        self.hideActivityIndicator()
                    }
                    
                    
            }
            
            
        }
    }
    
    
    func notAttendMeetingAPI() {
        if Reachability.isConnectedToNetwork() {
            
            var UserId = UserDefaults.standard.value(forKey: "USERID")
            let technicalIssue = "Reason"

            
            let passDictNotAttending = [
                
                
                "user_id": UserId,
                "meeting_id":1673,
                "latitude":75.02,
                "longitude":25.22,
                "reason": technicalIssue,

                
                ] as [String : Any]
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/attendMeeting", method: .post, parameters: passDictNotAttending, encoding: JSONEncoding.default )
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("msg",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                self.notAttendingArray = JSON[""] as? NSArray ?? []
                                print("THE ARRAY", self.notAttendingArray)
                                
                            }
                            
                        }
                        
                    case .failure(let error):
                        print("ERROR")
                        self.hideActivityIndicator()
                    }
                    
                    
            }
            
            
        }
    }
    
}
