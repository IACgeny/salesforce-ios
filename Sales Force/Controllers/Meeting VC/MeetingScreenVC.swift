//
//  MeetingScreenVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 28/10/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import GCCalendar

class MeetingScreenVC: UIViewController, GCCalendarViewDelegate{

    //MARK:- Outlet
    @IBOutlet weak var calenderView: GCCalendarView!
    @IBOutlet weak var lblMeetings: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var meetingView: UIView!
    
    //MARK:- USER DEFINE DATA
    let todayIdentity = UserDefaults.standard.value(forKey: "MeetingDayIdentity") as? String ?? ""
    var arrayRef:[Any] = []
    var meetingArray:[String] = []
    var btnTag:[Int] = []
    
    //MARK:- UIVIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        meetingLabelSetUp() //Seting the Value of Meeting Label from API
        calenderView.delegate = self
        calenderView.displayMode = .week
     
        
    }
    
   override func viewWillAppear(_ animated: Bool) {
        if arrayRef.count == 0{
            self.meetingView.isHidden = true
        }else{
            self.meetingView.isHidden = false
        }
        
       self.tableView.reloadData()
        
    }
    
    
    @IBAction func addMeeting(_ sender: Any) {
        let meetingVC = self.storyboard?.instantiateViewController(withIdentifier: "MeetingVC") as! MeetingVC
        self.navigationController?.pushViewController(meetingVC, animated: true)
    }
    
    
    @IBAction func btnBackToHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func meetingLabelSetUp() {
        if todayIdentity == "Today" {
            let todayStatus = UserDefaults.standard.value(forKey: "todayValue") as? Int ?? 0
            self.lblMeetings.text = "\(todayStatus) Meetings Today"
        }
        else if todayIdentity == "Week" {
            let weekStatus = UserDefaults.standard.value(forKey: "weekValue") as? Int ?? 0
            self.lblMeetings.text =  "\(weekStatus) Meeting This Week"
        }
        else if todayIdentity == "Month" {
            let monthStatus = UserDefaults.standard.value(forKey: "monthValue") as? Int ?? 0
            self.lblMeetings.text =  "\(monthStatus) Meeting This Month"
        }
    }
    
    
    func calendarView(_ calendarView: GCCalendarView, didSelectDate date: Date, inCalendar calendar: Calendar) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.calendar = calendar
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "dd MMMM yyyy", options: 0, locale: calendar.locale)
        
        self.navigationItem.title = dateFormatter.string(from: date)
        print("JHDAVGHEJVHV",dateFormatter.string(from: date))
    }
    

}


//MARK:- TableView Delegate and DataSource


extension MeetingScreenVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRef.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeetingTableViewCell", for: indexPath) as! MeetingTableViewCell
        
              let dic = arrayRef[indexPath.row] as! [String:Any]
        //cell.meetingDetails.text = dic["meeting_details"] as! String
        
        
        
        let client_name = dic["client_name"] as? String
        var str = "Client Name: "
        if let _ = client_name {
             str.append(client_name!)
        }
        cell.clientName.text = str
        
        
        let date_time = dic["date_time"] as? String
       var str2 = "Time: "
        if let _ = date_time {
            str2.append(date_time!)
        }
        cell.timeDay.text = str2
        
        
        let location = dic["location"] as? String
        var str3 = "Location: "
        if let _ = location {
            str3.append(location!)
        }
        cell.location.text = str3
        
        
        
        let agenda_other = dic["agenda_other"] as? String
        var str4 = "Subject: "
        if let _ = agenda_other {
            str4.append(agenda_other!)
        }
        cell.subjectOrAgenda.text = str4
        
        
        cell.attndngNotAttndng.text = dic["meeting_status"] as? String
        
        
        if cell.addSummary.tag == 0{
            cell.addSummary.isHidden = false
        }else{
            cell.addSummary.isHidden = true
        }
       cell.addSummary.addTarget(self, action: #selector(onAddSummaryClicked(_:)), for:.touchUpInside)
        return cell
    }
    
    
    @objc func onAddSummaryClicked(_ sender:UIButton){
       
        let summaryForm:summaryFormVC = self.storyboard?.instantiateViewController(withIdentifier: "summaryFormVC") as! summaryFormVC
        sender.tag = 1
        self.navigationController?.pushViewController(summaryForm, animated: true)
    }
    
}
