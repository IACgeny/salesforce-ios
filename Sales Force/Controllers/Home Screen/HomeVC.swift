//
//  HomeVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 14/10/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import Alamofire

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //slidebar
    var arrData = ["Home", "Meeting", "Meeting Summary", "Reimbursement", "leave management", "Daily Attendance", "Notification", "Profile", "Setting", "logout"]
    
    var arrImg = [#imageLiteral(resourceName: "home-1"), #imageLiteral(resourceName: "meeting"), #imageLiteral(resourceName: "meeting summary"), #imageLiteral(resourceName: "reimbursement"), #imageLiteral(resourceName: "leave"), #imageLiteral(resourceName: "leave"), #imageLiteral(resourceName: "notifications-m"), #imageLiteral(resourceName: "profile-m"), #imageLiteral(resourceName: "setting"), #imageLiteral(resourceName: "logout")]
    
    
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var sideBar: UITableView!
    
    var arrayMeeting:[Any] = []
    
    var isSideViewOpen: Bool = false

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SideBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SideBarTableViewCell
        cell.img.image = arrImg[indexPath.row]
        cell.lbl.text = arrData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    
    {
        if indexPath.row == 1 {
            
       
            let meetingScreen = self.storyboard?.instantiateViewController(withIdentifier: "MeetingScreenVC") as! MeetingScreenVC
            self.navigationController?.pushViewController(meetingScreen, animated: true)
            
        }
     
        else if indexPath.row == 2 {
            
            
                let summaryScreen =
                    self.storyboard?.instantiateViewController(withIdentifier: "summaryVC") as! summaryVC
                self.navigationController?.pushViewController(summaryScreen, animated: true)
            
        }
        
        else if indexPath.row == 3 {
            
            let rimbrsmntScreen = self.storyboard?.instantiateViewController(withIdentifier: "ReimbrsmntVC") as! ReimbrsmntVC
            self.navigationController?.pushViewController(rimbrsmntScreen, animated: true)
            
        }
        
        
        else if indexPath.row == 5 {
            
            let AttendanceSC = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceVC") as! AttendanceVC
            self.navigationController?.pushViewController(AttendanceSC, animated: true)
            
        }
        
        
    }
    
     
    func Slide () {
        sideView.isHidden = true
        sideBar.backgroundColor = UIColor.groupTableViewBackground
        isSideViewOpen = false
    }
   
    @IBAction func slideBarBtn(_ sender: Any) {
        
        sideBar.isHidden = false
        sideView.isHidden = false
        self.view.bringSubviewToFront(sideView)
        if !isSideViewOpen{
            isSideViewOpen = true//0
            sideView.frame = CGRect(x: 0, y: 75, width: 0, height: 511)
            sideBar.frame = CGRect(x: 0, y: 0, width: 0, height: 511)
            UIView.setAnimationDuration(0.3)
            UIView.setAnimationDelegate(self)
            UIView.beginAnimations("TableAnimation", context: nil)//1
            sideView.frame = CGRect(x: 0, y: 75, width: 281, height: 511)
            sideBar.frame = CGRect(x: 0, y: 0, width: 281, height: 511)
            UIView.commitAnimations()
            
        }else{
            sideBar.isHidden = true
            sideView.isHidden = true
            isSideViewOpen = false
            sideView.frame = CGRect(x: 0, y: 75, width: 281, height: 460)
            sideBar.frame = CGRect(x: 0, y: 0, width: 281, height: 460)
            UIView.setAnimationDuration(0.3)
            UIView.setAnimationDelegate(self)
            UIView.beginAnimations("TableAnimation", context: nil)
            sideView.frame = CGRect(x: 0, y: 75, width: 0, height: 460)
            sideBar.frame = CGRect(x: 0, y: 0, width: 0, height: 460)
            UIView.commitAnimations()
        }
        
    }
    

    
    // home, meeting
    @IBOutlet weak var todaysView: UIView!
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var monthView: UIView!
    
    @IBOutlet weak var lblTodayMeetingCount: UILabel!
    @IBOutlet weak var lblWeekMeetingCount: UILabel!
    @IBOutlet weak var lblMonthMeetingCount: UILabel!
  
    
    
    let devicetoken = "5311839E985FA01B56E7AD74334C0137F7D6AF71A22745D0FB50DED665E0E882"
    var userID = UserDefaults.standard.value(forKey: "USERID")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Slide()
        HomeAPICall()
        Dropshadow()
    }
    
    @IBAction func btnTodayMeeting(_ sender: Any) {
        
        let meetingsBy = "today"
        
        let passDictMeeting = [
            
            "user_id": userID,
            "meetings_by": meetingsBy] as [String : Any]
        
        if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/getAllMeetings", method: .post, parameters: passDictMeeting, encoding: JSONEncoding.default)
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any]
                        {
                            
                            self.hideActivityIndicator()
                            print("msg",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                UserDefaults.standard.set("Today", forKey: "MeetingDayIdentity") as? String ?? ""
                                
                                let meetingSC = self.storyboard?.instantiateViewController(withIdentifier: "MeetingScreenVC") as! MeetingScreenVC
                                meetingSC.arrayRef = JSON["result"] as![Any]
                                    self.navigationController?.pushViewController(meetingSC, animated: true)
                            }
                        }
                        
                    case .failure(let error):
                        print("ERROR")
                        self.hideActivityIndicator()
                    }
            }
            
            
        }
        
  
    }
    
    
    @IBAction func btnWeekMeetings(_ sender: Any) {
        
        
        
        let meetingsBy = "week"
        
        let passDictMeeting = [
            
            "user_id": userID,
            "meetings_by": meetingsBy] as [String : Any]
        
        if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/getAllMeetings", method: .post, parameters: passDictMeeting, encoding: JSONEncoding.default)
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any]
                        {
                            
                            self.hideActivityIndicator()
                            print("msg",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                             
                                UserDefaults.standard.set("Week", forKey: "MeetingDayIdentity") as? String ?? ""
                                
                                let meetingSC = self.storyboard?.instantiateViewController(withIdentifier: "MeetingScreenVC") as! MeetingScreenVC
                                self.navigationController?.pushViewController(meetingSC, animated: true)
                            }
                        }
                        
                    case .failure(let error):
                        print("ERROR")
                        self.hideActivityIndicator()
                        
                    }
            }
            
            
        }
        
        
    }
    
    @IBAction func btnMonthMeeting(_ sender: Any) {
        
        let MonthmeetingSC =  self.storyboard?.instantiateViewController(withIdentifier: "monthMeetingVC") as! monthMeetingVC
        self.navigationController?.pushViewController(MonthmeetingSC, animated: true)
      
    
    }
    
    
    @IBAction func addMeetingButton(_ sender: Any) {
        let MeetingVC = self.storyboard?.instantiateViewController(withIdentifier: "MeetingVC") as! MeetingVC
        self.navigationController?.pushViewController(MeetingVC, animated: true)
    }
    
    
    func Dropshadow() {
        todaysView.shadow(UIView: todaysView)
        weekView.shadow(UIView: weekView)
        monthView.shadow(UIView: monthView)
    }
    
    
    
}

extension UIView {
    
    func shadow(UIView: UIView) {
        UIView.center = self.center
        UIView.center = self.center
        UIView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        UIView.layer.shadowColor = UIColor.lightGray.cgColor
        UIView.layer.shadowOpacity = 1
        UIView.layer.shadowOffset = CGSize.zero
        UIView.layer.shadowRadius = 5
    }
}

extension HomeVC {
    
    func HomeAPICall() {
        
        let passDict = [
            "user_id": userID,
            "device_token": devicetoken] as [String : Any]
        
        
        if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/countMeetings", method: .post, parameters: passDict, encoding: JSONEncoding.default)
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("THE JSON RESULT of HOME SCREEN",JSON)
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                let todayMeeting = JSON["today_count"] as? Int ?? 0
                                let weekMeeting = JSON["week_count"] as? Int ?? 0
                                let monthMeeting = JSON["month_count"] as? Int ?? 0
                                
                                self.lblTodayMeetingCount.text = "\(todayMeeting)"
                                self.lblWeekMeetingCount.text = "\(weekMeeting)"
                                self.lblMonthMeetingCount.text = "\(monthMeeting)"
                                
                                UserDefaults.standard.set(todayMeeting, forKey: "todayValue")
                                UserDefaults.standard.set(weekMeeting, forKey: "weekValue")
                                UserDefaults.standard.set(monthMeeting, forKey: "monthValue")
                            }
                                
                            else {
                                self.hideActivityIndicator()
                                self.alert(message: JSON["msg"] as? String ?? "Invalid Creditions", title: "Sale Force Alert!!")
                                print("print error")
                            }
                        }
                    case .failure(let error):
                        print("ERROR")
                        self.hideActivityIndicator()
                        
                        self.alert(message: "Something went Wrong, Please Try Again", title: "Sales Force Alert!!")
                    }
                    
            }
        }
        else {
            self.hideActivityIndicator()
            alert(message: "No Internet Connection.", title: "Sales Force Alert!!!")
        }
        
        
    }
    
}
