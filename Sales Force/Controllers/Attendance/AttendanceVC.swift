//
//  AttendanceVC.swift
//  Sales Force
//
//  Created by Syed Zuber on 30/11/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import Alamofire
import MapKit
import CoreLocation


class AttendanceVC: UIViewController, CLLocationManagerDelegate {
    

    var manager = CLLocationManager()
    
    var lat: Double?
    var long: Double?
    var touchUntouch : Bool = true
    
    
    var  dictAttndnce = NSDictionary()
    
    @IBOutlet weak var lblidlePunchOut: UILabel!
    @IBOutlet weak var lblCurrentTime: UILabel!
    @IBOutlet weak var lblIntime: UILabel!
    @IBOutlet weak var lblOutTime: UILabel!
    @IBOutlet weak var btnPunch: UIButton!

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
  //      manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
     //   manager.startUpdatingLocation()
        getDateTime()
        getSingle()
        locc()
    }
    
    
    
   
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnPunch(_ sender: Any) {
        if touchUntouch == true{
            attndnceApiCall()
        }else{
            attendanceAlert(title: "PUNCH OUT", message: "Do You Want To Punch out?")
        }
        
        // let punchIN = UserDefaults.standard.value(forKey: "in_time")
        
        // if  punchIN != nil
        
        //{
        
        //}
        // else {
        
        ///}
    }
    
    func getDateTime() {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .medium
        let str = formatter.string(from: Date())
        lblCurrentTime.text = str
    }

    func getSingle() {
        let date = Date()
        let calender = Calendar.current
        let hours = calender.component(.hour, from: date)
        let minutes = calender.component(.minute, from: date)
        let seconds = calender.component(.second, from: date)
         print ("\(hours):\(minutes):\(seconds)")
        var currentTime = ("\(hours):\(minutes):\(seconds)")
         //self.lblidlePunchOut.text = currentTime
    }
    
    
    func locc() {
        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                manager.requestWhenInUseAuthorization()
            }
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.delegate = self
            manager.startUpdatingLocation()

        } else {
            print("Please turn on location services or GPS")
        }
      
    }
    
    //MARK:- CLLocationManager Delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.manager.stopUpdatingLocation()
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002))
            let latitude = region.center.latitude
            let long = region.center.longitude
            self.lat = latitude
            self.long = long
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Unable to access your current location")
    }
    

    
    // Mark :- Attendance API Call.
    func attndnceApiCall() {
        if Reachability.isConnectedToNetwork() {
            
           /* var currentLocation: CLLocation!


            if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways){
                
                currentLocation = locManager.location
                
            var  longitude  = "\(currentLocation.coordinate.longitude)"
            var   latitude = "\(currentLocation.coordinate.latitude)"
            var UserId = UserDefaults.standard.value(forKey: "USERID")*/

            var UserId = UserDefaults.standard.value(forKey: "USERID")
            var location = "Location"
            
            let passDictAtndnce = [
                "user_id": UserId,
                "lat_in":"\(self.lat)",
                "long_in":"\(self.long)",
                "lat_out":"\(self.lat)",
                "long_out":"\(self.long)",
                "location_in": location,
                "location_out": location
                ] as [String : Any]
            
            showActivityIndicator()
            Alamofire.request("http://13.232.6.230/salesforce-dev/public/api/saveAttendance", method: .post, parameters: passDictAtndnce, encoding: JSONEncoding.default )
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            self.hideActivityIndicator()
                            print("msg",JSON)
                            let status = JSON["status"] as? String ?? ""
                            print(status)
                            if status == "success" {
                                self.touchUntouch = false
                                self.hideActivityIndicator()
                                let inTime = JSON["in_time"] as? String ?? ""
                                let outTime = JSON["out_time"] as? String ?? ""
                                UserDefaults.standard.set(inTime, forKey: "inTimeKey")
                                UserDefaults.standard.set(inTime, forKey: "outTimeKey")
                                self.lblIntime.text = String(describing: inTime.suffix(8))
                                self.lblOutTime.text = String(describing: outTime.suffix(8))
                        
                                //let date = inTime.addingTimeInterval(TimeInterval(510 * 60.0))
                                //print(time)
                            }
                            else{
                                print("else")
                            }
                        }
                    case .failure(let error):
                        print("ERROR", error.localizedDescription)
                        self.hideActivityIndicator()
                    }
               }
          }
     }
    
    //Mark:- Alert Box
    
    func attendanceAlert (title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
            //alert.dismiss(animated: true, completion: nil)
            self.attndnceApiCall()
            self.btnPunch.isUserInteractionEnabled = false
        }))
        alert.addAction(UIAlertAction(title: "cancel", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

