//
//  LoginViewController.swift
//  Sales Force
//
//  Created by Syed Zuber on 13/10/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit
import Alamofire
import Firebase


class LoginViewController: UIViewController {
    
    //MARK:- IBOutlets
    //MARK:-
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    let devicetoken = "5311839E985FA01B56E7AD74334C0137F7D6AF71A22745D0FB50DED665E0E882"
    
    var dataDict = NSDictionary()
    
    //MARK:- View Life Cycles
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dissmissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    @objc func dissmissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK:- IB Actions Buttons
    //MARK:-
    @IBAction func loginClicked(_ sender: Any) {
        validationsTextFields()
    }
    
    @IBAction func forgotClicked(_ sender: Any) {
    }
    
    
}
//MARK:- WebServices API Call
//MARK:-
extension LoginViewController {
    
    func logIn() {
        
        let passDict = [
            "email": userNameTextField.text!,
            "password":passWordTextField.text!,
            "device_token": devicetoken] as [String : Any]
        
        
        if Reachability.isConnectedToNetwork() {
            
            showActivityIndicator()
            
            Alamofire.request("http://13.232.6.230/SalesForce/public/api/login", method: .post, parameters: passDict, encoding: JSONEncoding.default)
                
                .responseJSON { (response) in
                    
                    switch response.result {
                        
                    case .success:
                        if let JSON = response.result.value as? [String: Any] {
                            
                            self.hideActivityIndicator()
                            
                            print("THE JSON RESULT",JSON)
                            
                            let message = JSON["status"] as? String ?? ""
                            print(message)
                            if message == "success" {
                                
                                self.hideActivityIndicator()
                                
                                self.dataDict = JSON["user"] as? NSDictionary ?? [:]
                                print("User Details of from Login Response", self.dataDict)
                                
                                let userID = self.dataDict.value(forKey: "user_id") as? Int ?? 0
                                UserDefaults.standard.set(userID, forKey: "USERID")
                                UserDefaults.standard.synchronize()
                                
                                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                self.navigationController?.pushViewController(homeVC, animated: true)
                            }
                                
                            else {
                                self.hideActivityIndicator()
                                
                                self.alert(message: JSON["msg"] as? String ?? "Invalid Creditions", title: "Sale Force Alert!!")
                                print("print error")
                            }
                        }
                    case .failure(let error):
                        print("ERROR")
                        self.hideActivityIndicator()
                        
                        self.alert(message: "Something went Wrong, Please Try Again", title: "Sales Force Alert!!")
                    }
                    
            }
        }
        else {
            self.hideActivityIndicator()
            alert(message: "No Internet Connection.", title: "Sales Force Alert!!!")
        }
        
        
    }
    
    
    func validationsTextFields() {
        
        if (userNameTextField.text?.isEmpty == true) && (passWordTextField.text?.isEmpty == true) {
            
            alert(message: "Please Fill the details", title: "Sales Force Alert!!")
        }
            
        else if (userNameTextField.text?.isEmpty == true) {
            
            alert(message: "Please Fill the User Name", title: "Sale Force Alert!!")
        }
            
        else if (passWordTextField.text?.isEmpty == true) {
            
            alert(message: "Please Fill the Password", title: "Sale Force Alert!!")
        }
            
        else {
            logIn() // Call the API
        }
    }
    
    
}




