//
//  SideBarTableViewCell.swift
//  Sales Force
//
//  Created by Syed Zuber on 31/10/18.
//  Copyright © 2018 Indian Auto Company. All rights reserved.
//

import UIKit

class SideBarTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
